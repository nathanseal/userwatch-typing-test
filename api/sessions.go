package main

import (
	"errors"
	"math"
	"math/rand"
	"sort"
	"sync"
	"time"

	"github.com/google/uuid"
)

var textList = []string{
	"Who washes the windows by Harold the fox.",
	"The quick brown fox washes the dishes and stares out Wendy's window.",
	"Humpdy Dumpty washes windows and jumps over the wall.",
	"Windows by the sea shore require regular washes to see out.",
}

// This is the main data that the server is storing across requests. The mutex is to avoid race
// conditions but otherwise it is a map of sessions.
type ServerDataModel struct {
	Sessions map[string]TypingSession `json:"sessions"`
	mu       sync.Mutex
}

type TypingSession struct {
	Id              string        `json:"id"`
	Name            *string       `json:"name"`
	TargetText      string        `json:"targetText"`
	Events          []EventLog    `json:"events"`
	Done            bool          `json:"done"`
	FinalText       *string       `json:"finalText"`
	StartTime       int64         `json:"startTime"`
	DeviceStartTime int64         `json:"deviceStartTime"`
	EndTime         *int64        `json:"endTime"`
	DeviceEndTime   *int64        `json:"deviceEndTime"`
	Stats           *SessionStats `json:"stats"`
}

// A log to record a single event this is mostly a type of an individual letter but other events
// like deletions also get recorded. The device time and server time should mostly be similar but
// may in some cases be interesting to observe differences.
type EventLog struct {
	DeviceTime  int64  `json:"deviceTime"`
	ServerTime  int64  `json:"serverTime"`
	EventType   string `json:"type"`
	EventLetter string `json:"letter"`
}

type SessionStats struct {
	TypingDuration         int64              `json:"typingDuration"`
	AverageTimePerLetter   float64            `json:"averageTimePerLetter"`
	TimeSpentTypingLetters int64              `json:"timeSpentTypingLetters"`
	LettersWritten         []LetterEventStats `json:"lettersWritten"`
}

type LetterEventStats struct {
	Letter string `json:"letter"`
	Time   int64  `json:"time"`
}

type SimilarityItem struct {
	SimilarityScore float64 `json:"score"`
	Name            string  `json:"name"`
	Id              string  `json:"Id"`
}

// BySimilarity is used to sort a slice of similarity results by their score.
type BySimilarity []SimilarityItem

func (a BySimilarity) Len() int           { return len(a) }
func (a BySimilarity) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a BySimilarity) Less(i, j int) bool { return a[i].SimilarityScore > a[j].SimilarityScore }

func NewServerDataModel() ServerDataModel {
	return ServerDataModel{make(map[string]TypingSession), sync.Mutex{}}
}

func (s *ServerDataModel) InitSession(deviceTime int64) TypingSession {
	s.mu.Lock()
	defer s.mu.Unlock()
	targetText := textList[rand.Intn(len(textList))]
	session := TypingSession{
		Id:              uuid.New().String(),
		Name:            nil,
		TargetText:      targetText,
		Events:          make([]EventLog, 0),
		Done:            false,
		FinalText:       nil,
		StartTime:       time.Now().UnixMilli(),
		DeviceStartTime: deviceTime,
		EndTime:         nil,
		DeviceEndTime:   nil,
	}
	s.Sessions[session.Id] = session
	return session
}

func (s *ServerDataModel) RecordEvent(sessionId string, eventType string, letter string, deviceTime int64) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	currentSession, ok := s.Sessions[sessionId]
	if !ok || currentSession.Done {
		return errors.New("invalid session id")
	}
	event := EventLog{deviceTime, time.Now().UnixMilli(), eventType, letter}
	currentSession.Events = append(currentSession.Events, event)
	s.Sessions[sessionId] = currentSession
	return nil
}

func (s *ServerDataModel) SubmitSession(sessionId string, name string, text string, deviceTime int64) ([]SimilarityItem, error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	currentSession, ok := s.Sessions[sessionId]
	var similarities = make([]SimilarityItem, 0)

	if !ok || currentSession.Done {
		return nil, errors.New("invalid session id")
	}

	currentSession.Name = &name
	currentSession.FinalText = &text
	currentSession.Done = true
	endTime := time.Now().UnixMilli()
	currentSession.EndTime = &endTime
	currentSession.DeviceEndTime = &deviceTime
	stats := calculateStats(currentSession)
	currentSession.Stats = &stats

	for _, session := range s.Sessions {
		if session.Stats != nil && session.Name != nil {
			currentSimilarity := calculateSimilarity(stats, *session.Stats)
			similarities = append(similarities, SimilarityItem{currentSimilarity, *session.Name, session.Id})
		}
	}

	s.Sessions[sessionId] = currentSession

	// Return the top five best results
	sort.Sort(BySimilarity(similarities))
	var topFive []SimilarityItem
	if len(similarities) > 5 {
		topFive = similarities[:5]
	} else {
		topFive = similarities
	}
	return topFive, nil
}

func (s *ServerDataModel) GetAllSessions() []TypingSession {
	s.mu.Lock()
	defer s.mu.Unlock()
	result := make([]TypingSession, 0)
	for _, v := range s.Sessions {
		result = append(result, v)
	}
	return result
}

// Returns a number from [100, 0) depending how similar the two sessions are.
func calculateSimilarity(one SessionStats, two SessionStats) float64 {
	diff := math.Abs(one.AverageTimePerLetter - two.AverageTimePerLetter)
	return 100 / math.Pow(2, diff/100)
}

func calculateStats(session TypingSession) SessionStats {
	stats := SessionStats{}
	for i, event := range session.Events {
		if event.EventType == "insertText" {
			var timeTakenToType int64 = 0
			if i > 0 {
				// If there is no previous typing event then assume the letter took no time to type.
				previousEvent := session.Events[i-1]
				timeTakenToType = event.DeviceTime - previousEvent.DeviceTime
			}
			letterStats := LetterEventStats{event.EventLetter, timeTakenToType}
			stats.LettersWritten = append(stats.LettersWritten, letterStats)
			stats.TimeSpentTypingLetters += timeTakenToType
		}
	}

	if len(session.Events) > 0 {
		stats.TypingDuration = session.Events[len(session.Events)-1].DeviceTime - session.Events[0].DeviceTime
	}
	if len(stats.LettersWritten) > 0 {
		stats.AverageTimePerLetter = float64(stats.TimeSpentTypingLetters) / float64(len(stats.LettersWritten))
	}

	return stats
}
