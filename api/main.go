package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"
)

func main() {
	port := "5000"

	dataModel := NewServerDataModel()

	http.HandleFunc("/init", dataModel.handleInit)
	http.HandleFunc("/event", dataModel.handleEvent)
	http.HandleFunc("/submit", dataModel.handleSubmit)
	http.HandleFunc("/data", dataModel.handleGetAllData)
	fmt.Printf("Running... http://localhost:%v\n", port)
	http.ListenAndServe(fmt.Sprintf(":%v", port), nil)
}

// Handler method for the /init endpoint
func (dm *ServerDataModel) handleInit(w http.ResponseWriter, r *http.Request) {
	log.Printf("%v %v", r.Method, r.URL.Path)

	deviceTime, err := strconv.ParseInt(r.FormValue("time"), 10, 0)
	if err != nil {
		log.Println(err)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	session := dm.InitSession(deviceTime)

	jsonResponse(w, map[string]any{
		"time":       time.Now().Unix(),
		"status":     "success",
		"sessionId":  session.Id,
		"targetText": session.TargetText,
	})
}

// Handler method for the /event endpoint
func (dm *ServerDataModel) handleEvent(w http.ResponseWriter, r *http.Request) {
	log.Printf("%v %v", r.Method, r.URL.Path)
	sessionId := r.FormValue("sessionId")
	eventType := r.FormValue("type")
	letter := r.FormValue("letter")
	deviceTime, err := strconv.ParseInt(r.FormValue("time"), 10, 0)
	if err != nil {
		log.Println(err)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	err = dm.RecordEvent(sessionId, eventType, letter, deviceTime)
	if err != nil {
		log.Println("Error recording event", err)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	jsonResponse(w, map[string]any{
		"time":   time.Now().Unix(),
		"status": "success",
	})
}

// Handler method for the /submit endpoint
func (dm *ServerDataModel) handleSubmit(w http.ResponseWriter, r *http.Request) {
	log.Printf("%v %v", r.Method, r.URL.Path)

	sessionId := r.FormValue("sessionId")
	name := r.FormValue("name")
	text := r.FormValue("text")
	deviceTime, err := strconv.ParseInt(r.FormValue("time"), 10, 0)
	if err != nil {
		log.Println(err)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}
	if name == "" || text == "" {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	similarities, err := dm.SubmitSession(sessionId, name, text, deviceTime)
	if err != nil {
		log.Println("Error submitting session", err)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	if len(similarities) > 0 {
		mostSimilar := similarities[0]
		jsonResponse(w, map[string]any{
			"time":         time.Now().Unix(),
			"status":       "success",
			"message":      fmt.Sprintf("You are most similar to %v", mostSimilar.Name),
			"similarities": similarities,
		})
	} else {
		jsonResponse(w, map[string]any{
			"time":    time.Now().Unix(),
			"status":  "success",
			"message": "There are no other people you are similar to",
		})
	}
}

// Handler method for the /data endpoint
func (dm *ServerDataModel) handleGetAllData(w http.ResponseWriter, r *http.Request) {
	log.Println(r.Method, r.URL.Path)
	sessions := dm.GetAllSessions()
	jsonResponse(w, sessions)
}

func jsonResponse(w http.ResponseWriter, v any) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err := json.NewEncoder(w).Encode(v)
	if err != nil {
		log.Println("Json error", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}
}
