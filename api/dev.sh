while true; do
	go run .& 
	PID=$!
	echo PID=$PID
	inotifywait -e close_write $(find | grep go)
	pkill -P $PID
done
