
const apiRoot = '/api'

/**
 * Initialise a new typing session. This will return an object with a session id and a piece of text
 * for the user to write.
 */
function init () {
  const formData = new FormData()
  formData.append('time', new Date().getTime())
  return fetch(apiRoot + '/init', {
    method: 'POST',
    body: formData
  }).then((response) => response.json())
}

/**
 * Submits the typing session to the api. Once called no more input should be sent. Will return some
 * results from the api on what other users are similar.
 */
function submit (sessionId, name, text) {
  const formData = new FormData()
  formData.append('sessionId', sessionId)
  formData.append('name', name)
  formData.append('text', text)
  formData.append('time', new Date().getTime())
  return fetch(apiRoot + '/submit', {
    method: 'POST',
    body: formData
  }).then((response) => response.json())
}

/**
 * Record an event with the api. This is for when a user is typing and we want to record what they
 * are doing for performing statistical analysis.
 */
function event (sessionId, type, letter) {
  const formData = new FormData()
  formData.append('sessionId', sessionId)
  formData.append('type', type)
  formData.append('letter', letter)
  formData.append('time', new Date().getTime())
  return fetch(apiRoot + '/event', {
    method: 'POST',
    body: formData
  }).then((response) => response.json())
}

export default { init, submit, event }
