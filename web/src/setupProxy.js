const { createProxyMiddleware } = require('http-proxy-middleware')

module.exports = (app) => {
  // This is what sets up a reverse proxy so that /api/* calls to the node server running this react
  // app will get forwarded to the api.
  const apiHost = process.env.REACT_APP_API_ROOT || 'http://localhost:5000'
  console.log('apiHost', apiHost)

  app.use(
    '/api',
    createProxyMiddleware({
      target: apiHost,
      changeOrigin: true,
      pathRewrite: {
        '^/api': ''
      }
    })
  )
}
