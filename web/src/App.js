import React, { Component } from 'react'
import api from './api'
import './App.css'

class App extends Component {
  constructor (props) {
    super(props)

    this.state = {
      sessionId: null, // The session id of the current input session
      targetText: null, // The text we tell the user to write
      name: '', // The name the user has entered
      text: '', // the text the user has typed in the text box
      output: '', // an output message from the server
      similarities: null // an array of similar users
    }

    this.onNameChange = this.onNameChange.bind(this)
    this.onTextChange = this.onTextChange.bind(this)
    this.onTextInput = this.onTextInput.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount () {
    this.initSession()
  }

  initSession () {
    api.init().then((data) => {
      this.setState({
        sessionId: data.sessionId,
        targetText: data.targetText
      })
    })
  }

  /**
   * Called by the onChange callback for the user name input text element.
   */
  onNameChange (event) {
    this.setState({
      name: event.currentTarget.value
    })
  }

  /**
   * Called by the onChange callback for the main textarea. Use to update the state of this
   * component.
   */
  onTextChange (event) {
    this.setState({
      text: event.currentTarget.value
    })
  }

  /**
   * Called whe there is an input event for the main text area. This is slightly different from the
   * onChange callback as unlike onChange it gets called for every alteration. For recording input
   * events we do specifically want every alteration hence the reason for this callback.
   *
   * https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/oninput
   */
  onTextInput (event) {
    const type = event.nativeEvent.inputType
    const letter = event.nativeEvent.data
    api.event(this.state.sessionId, type, letter)
  }

  handleSubmit (event) {
    event.preventDefault()

    this.setState({
      output: 'loading...'
    }, () => {
      api.submit(this.state.sessionId, this.state.name, this.state.text).then((data) => {
        this.setState({
          name: '',
          text: '',
          output: data.message,
          similarities: data.similarities,
          sessionId: null,
          targetText: null
        }, () => {
          this.initSession()
        })
      })
        .catch((error) => {
          console.log('handleSubmit', error)
          this.setState({
            output: 'error'
          })
        })
    })
  }

  /**
   * Not all that much to the ui. We show a form for input and some output text as output based on
   * responses from the api.
   */
  render () {
    let similarityList = null
    if (this.state.similarities) {
      similarityList = this.state.similarities.map((item) =>
        <li key={item.id} >{item.name} - {item.score.toFixed(2)}</li>
      )
    }

    return (
      <div className="App">
        <div className="App-header">
          <h2>Nathan&apos;s typing test</h2>
        </div>
        <div className="App-intro">
          {this.state.sessionId &&
            <form onSubmit={this.handleSubmit} >
              <p>Type your name here:</p>
              <input type="text" name="name" value={this.state.name} onChange={this.onNameChange} />
              <p>Then type the following text in the text area and submit:</p>
              <p><code>{this.state.targetText}</code></p>
              <textarea name="textInput" value={this.state.text} onChange={this.onTextChange} onInput={this.onTextInput} />
              <p>
                <button type="submit">submit</button>
              </p>
            </form>
          }

          <p>{this.state.output}</p>

          {similarityList &&
            <div>
              <p>Here are some more people you are similar too and their similarity score</p>
              <ol>
                {similarityList}
              </ol>
            </div>
          }
        </div>
      </div>
    )
  }
}

export default App
