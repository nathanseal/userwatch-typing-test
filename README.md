# Userwatch Typing Test

The Userwatch Typing Test is a quick little app that allows users to type in some text and see who they are most similar to when typing.
Even though there is a specific bit of text we tell the user to write there is no checking that they actually write it correctly.
I am more concerned with how they write the text than what they actually write.

# Architecture

The main architecture of the app is to use go for a backend api and a react project for the web frontend.
All the data stored in the api is in memory so it gets lost whenever the server restarts.
This is a little annoying but fine for a quick demo project.

```
+-----+     +-----+
| Web | --> | Api |
+-----+     +-----+
```

# Running the app

## Dependencies

Always good to make sure you have updated versions of tools installed.
Currently I have the following list of versions.
Other versions may work but I have not tested everything.
Docker is only needed if you want to run with docker but if you run with docker it should deal with all the other dependencies.

* Go - 1.18.1
* node - 18.4.0
* npm - 8.12.2
* docker - 20.10.14
* docker-compose - 1.29.2

## Standard

Start up the api with:

```
cd api
go run .
```

The api should now be running on `http://localhost:5000`

For dev things you can also run the server with `./dev.sh` instead of `go run .` this does live reloading of the server when go files are updated which is nice when making quick changes.

Then in another shell start the react web app with:

```
cd web
npm install
npm start
```

The web front end should now be running on `http://localhost:3000`.
Also since the front end is running a proxy to the backend the api can also be reached on `http://localhost:3000/api`.

## Docker

If you want to run with docker you can start up both servers with docker using the following.

```
docker-compose up --build -d
```

# Api

The api has a few end points as follows. 

* `POST /init` - Start a new session, will respond with a session id and the text to type.
* `POST /event` - Posts an event to the server to log an action by the user.
* `POST /submit` - Submits the current session. Responds with information on the most similar users.
* `GET /data` - Gets all the session data stored on the server. Useful for debugging.

The basic idea is to call `/init` to start a session then call `/event` a bunch of times while typing and finally call `/submit` to end the session and calculate some stats.

# Similarity calculations

The goal of this app it to detect similarity between users and show who a user is most similar to.
Obviously we could do all kinds of fancy algorithms for this but spending months on machine learning algorithms is not really my goal here.

At the moment the algorithm uses essentially the following steps.

* First collect data by logging all the input events on the text area with timings.
* Then process the data by using the timings to calculate the average time it takes the user to type a key. The time taken to type a key is the difference in time from when the key was typed and the last input event. Note that the last input event may not always be entering a letter such as when the user deletes text from any typos.
* Finally calculate some score for a particular submission. A user is considered similar to an other user if the difference between the average key press times for two typing sessions is similar. The difference is scaled and mapped a little to make 100 the maximum and 0 the minimum. More specifically the mapping is `100 / (2 ^ (|avg1 - avg2| / 100))` although scaling is not particularly important I just wanted numbers within a range.

```
+-------------------+     +-------------------+     +------------------+
|   Collect data    | --> |   Process Data    | --> | Calculate Scores |
+-------------------+     +-------------------+     +------------------+
```

You could calculate average key type times in other ways such as a basic total session time divided by the number of characters in the final submission. This may provide a similar result but I think that being able to lok at the distribution of times may be interesting in the future.
For example I made a histogram of times taken to type a letter for one session I did.
The data and chart are at this google sheet link.
https://docs.google.com/spreadsheets/d/1CVQexGi0-d_g2BfS6zkLfzP7lWgI2DrdS1njUuDsbMA/edit?usp=sharing
You could imagine that comparing the distributions in different ways could lead to better results.

# Future

There is a lot more that could be done on a project like this.
At the moment the similarity algorithm being used it just an arbitrary thing I came up with and I have no particular evidence to know if it is good or not.
To get a better algorithm we should first collect some more data.
We also should store this data persistently so I don't need to worry about losing it every time the server restarts and we can use it for offline calculations and tests.

You may notice that much of the data the server is currently collecting about the typing session is not currently being used in the similarity algorithm.
This is because I don't necessarily know what is important but if I capture it all I can then figure out what is important later.

After collecting some data before trying to make a good algorithm we need a way to measure if what we are doing is working or not.
This could involve some kind of check to see what percent of user sessions an algorithm can correctly identify the user for.
Any test data for this should be kept separate from any training data to avoid over fitting.

Some more ideas off the top of my head for ways to get more interesting results from an algorithm.

* Look at ways to clean the data from outliers. eg if the user gets distracted and stops typing for a bit that makes it look like they are slow at typing. (medians, percentiles, and standard deviations could help with this)
* Look at the time it takes the user to type a specific letters rather than treating each letter the same.
* Analyse things like mistake frequency and how much a user pauses or deletes text
* Get a bunch of the dimensions apply some weights and adjust the weights either by instinct or machine leaning with some training data.

Although if you really want it to work well you should consider asking an expert or maybe spend some time doing some machine learning training to get a bit of a better idea of what algorithms are out there for this kind of thing.